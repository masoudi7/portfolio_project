# Generated by Django 3.1 on 2020-08-25 15:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Home', '0008_my_resume'),
    ]

    operations = [
        migrations.AddField(
            model_name='my_resume',
            name='first_ability_name',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='my_resume',
            name='first_ability_percentage',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='my_resume',
            name='forth_ability_name',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='my_resume',
            name='forth_ability_percentage',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='my_resume',
            name='second_ability_name',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='my_resume',
            name='second_ability_percentage',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='my_resume',
            name='third_ability_name',
            field=models.CharField(default='', max_length=50),
        ),
        migrations.AddField(
            model_name='my_resume',
            name='third_ability_percentage',
            field=models.IntegerField(default=1),
        ),
    ]
