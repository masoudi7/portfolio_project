from django.shortcuts import render
from.models import home_page
from.models import info_call
from.models import my_resume
# Create your views here.
def home(request):
    home_page_data=home_page.objects.all()
    info_call_data=info_call.objects.all()
    my_resume_data=my_resume.objects.all()
    return render(request,'Home/index.html',{'home_page_data':home_page_data,'info_call_data':info_call_data,'my_resume_data':my_resume_data})